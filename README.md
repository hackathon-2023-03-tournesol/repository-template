# A repository template

A template that can be used to create new repositories for the hackathon
2023-03 organized by the associations Data For Good and Tournesol.

This repository aims to provide a common foundation to all projects
developed during the hackathon. We hope this template will invite
participants to create documented tools in replicable environments.

## Usage

Copy the `README.template.md` into your repository, rename it `README.md`.

Delete the examples, and complete the different sections. Don't forget to
choose at license.
