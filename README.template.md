# Name of the project

Short and explicit description.

A long description (optional) can be used to give more details..

## Table of Content

- [Install](#install)
- [Usage](#usage)
- [License](#license)

## Install

**Status:** required

Document here how to set up a working environment. System dependencies should
be listed first.

**Example:**

Requirements:
- Git
- Python >= 3.9
- virtualenvwrapper


```
git clone ...
cd project_name

mkvirtualenv project_name
pip install -r requirements.txt
```

## Usage

**status:** required

Document here how run the project.

If the project provides one or more commands, give some relevant examples.

**Example:**

```bash
$ python3 src/build_correlation_matrix.py --input file.csv
```

To filter by user, use the `--user` option.

```bash
$ python3 src/build_correlation_matrix.py --input file.csv --user hello
```

To display all options.

```bash
$ python3 src/build_correlation_matrix.py --help
```

## License

**Status:** required

We encourage everyone to develop Free/Libre software.

Write here the disclaimer of your chosen license and create a `LINCENSE.txt`
file containing the full license text.

Consider using the GNU General Public License v3.0 or later:
https://www.gnu.org/licenses/gpl-3.0.en.html

**Example** with GPLv3 or later:

Copyright 2023 Alice, Bob and contributors.

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
    License for more details.

    You should have received a copy of the GNU General Public License along
    with this program. If not, see <https://www.gnu.org/licenses/>.
